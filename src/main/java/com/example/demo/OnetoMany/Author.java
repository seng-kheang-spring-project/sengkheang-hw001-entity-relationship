package com.example.demo.OnetoMany;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tblAuthor")
public class Author {

    @Id
    @Column(name = "Id")
    @GeneratedValue
    private Integer id;

    @Column(name = "author_name")
    private String authorName;

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", authorName='" + authorName + '\'' +
                '}';
    }

    @OneToMany(mappedBy = "author",targetEntity = Book.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Book> books;

    public Author(String authorName, List<Book> books) {
        this.authorName = authorName;
        this.books = books;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Author() {
    }
}
