package com.example.demo.controller;

import com.example.demo.ManyToMany.Product;
import com.example.demo.ManyToMany.Supplier;
import com.example.demo.repository.ProductCrudRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;

@Controller
public class ProductController {

    @Autowired
    private ProductCrudRepo productCrudRepo;

    @GetMapping("/addProduct")
    @ResponseBody
    public String addProduct(){
        Product product=new Product();
        Supplier supplier=new Supplier();

        supplier.setSupplierName("Mr.Wen");
        product.setProductName("External SSD");

        product.setSuppliers(Collections.singletonList(supplier));
        productCrudRepo.save(product);

        return "Add Product Successful!!!";
    }

    @GetMapping("/findProductAll")
    @ResponseBody
    public String findProductAll(){
        return "" + productCrudRepo.findAll();
    }

    @GetMapping("/findProduct/{id}")
    @ResponseBody
    public String findProductById(@PathVariable Integer id){
        return "" + productCrudRepo.findById(id).get();
    }

    @GetMapping("/updateProduct/{id}")
    @ResponseBody
    public String updateProduct(@PathVariable Integer id){
        Product product=new Product();
        product=productCrudRepo.findById(id).get();

        product.setProductName("Mi6_Robot");
        productCrudRepo.save(product);
        return "update product successful!!!";
    }

    @GetMapping("/deleteProduct/{id}")
    @ResponseBody
    public String deleteProduct(@PathVariable Integer id){
      Product product=new Product();
      product=productCrudRepo.findById(id).get();

      productCrudRepo.delete(product);
        return "Delete Product Successful!!!";
    }

}
